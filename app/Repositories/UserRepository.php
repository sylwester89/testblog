<?php

namespace App\Repositories;

interface UserRepository
{
    function model();
}