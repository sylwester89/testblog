<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class CommentEloquentRepository extends BaseRepository implements CommentRepository
{
    function model()
    {
        return "App\\Comment";
    }
}