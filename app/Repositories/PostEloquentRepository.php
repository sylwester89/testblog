<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class PostEloquentRepository extends BaseRepository implements PostRepository
{
    public function allRestricted()
    {
        $columns = ['title'];

        if(auth()->check())
        {
            $columns = array_merge(['id', 'description', 'created_at'], $columns);
        }

        return $this->all($columns);
    }

    function model()
    {
        return "App\\Post";
    }
}