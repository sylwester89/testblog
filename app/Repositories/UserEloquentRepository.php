<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class UserEloquentRepository extends BaseRepository implements UserRepository
{
    function model()
    {
        return "App\\User";
    }
}