<?php

namespace App\Repositories;

interface CommentRepository
{
    function model();
}