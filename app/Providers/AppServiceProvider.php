<?php

namespace App\Providers;

use App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'admin.*', 'App\Http\ViewComposers\AdminComposer'
        );

        view()->composer(
            'frontend.*', 'App\Http\ViewComposers\MenuComposer'
        );

        view()->composer(
            ['frontend.*', 'admin.posts.index'], 'App\Http\ViewComposers\SupplyComposer'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('App\Repositories\UserRepository', 'App\Repositories\UserEloquentRepository');
        App::bind('App\Repositories\PostRepository', 'App\Repositories\PostEloquentRepository');
        App::bind('App\Repositories\CommentRepository', 'App\Repositories\CommentEloquentRepository');

        if ($this->app->environment() !== 'production')
        {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
