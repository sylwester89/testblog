<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ResetPasswordForm extends Form
{
    protected $options = [
        'method' => 'POST'
    ];

    public function getClassName()
    {
        return self::class;
    }

    public function getOptions($type)
    {
        $this->options['type'] = $type;

        switch ($type)
        {
            case "linkRequest":
                $this->options['url'] = route('password.email');
                $this->options['class'] = 'form-horizontal';
                break;
            case "resetPassword":
                $this->options['url'] = route('password.request');
                $this->options['class'] = 'form-horizontal';
                break;
        }

        return $this->options;
    }

    public function buildForm()
    {
        switch ($this->getFormOption('type'))
        {
            case "resetPassword":
                $this->buildResetPassword();
                break;
            default:
                $this->buildDefault();
        }
    }

    protected function buildDefault()
    {
        $this
            ->add('email', 'email', [
                'rules' => 'required|email|min:6|max:60',
                'label'    => 'E-Mail Address',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Send Password Reset Link',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }

    protected function buildResetPassword()
    {
        $this
            ->add('email', 'email', [
                'rules' => 'required|email|min:6|max:60',
                'label'    => 'E-Mail Address',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('password', 'password', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Password',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('password_confirmation', 'password', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Confirm Password',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Reset password',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
}