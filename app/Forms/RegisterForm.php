<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class RegisterForm extends Form
{
    protected $options = [
        'method' => 'POST'
    ];

    public function getClassName()
    {
        return self::class;
    }

    public function getOptions()
    {
        $this->options['url'] = route('register');
        $this->options['class'] = 'form-horizontal';

        return $this->options;
    }

    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2|max:60',
                'label'    => 'Name',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'maxlength' => 60,
                    'placeholder' => 'John Doe'
                ]
            ])
            ->add('email', 'email', [
                'rules' => 'required|emmail|min:6|max:60',
                'label'    => 'E-Mail Address',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60,
                    'placeholder' => 'john@gmail.com'
                ]
            ])
            ->add('password', 'password', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Password',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('password_confirmation', 'password', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Confirm Password',
                'label_attr'    => [
                    'class' => 'col-md-6 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Register',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
}