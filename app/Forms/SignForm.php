<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SignForm extends Form
{
    protected $options = [
        'method' => 'POST'
    ];

    public function getClassName()
    {
        return self::class;
    }

    public function getOptions($type)
    {
        switch ($type)
        {
            case "login":
                $this->options['url'] = route('login');
                break;
            case "logout":
                $this->options['url'] = route('logout');
                break;
        }

        return $this->options;
    }

    public function buildForm()
    {
        switch ($this->getFormOption('type'))
        {
            case "logout":
                $this->buildLogout();
                break;
            default:
                $this->buildDefault();
        }
    }

    public function buildDefault()
    {
        $this
            ->add('email', 'email', [
                'rules' => 'required|email|min:6|max:60',
                'label'    => 'E-Mail :',
                'label_attr'    => [
                    'class' => 'col-md-4 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('password', 'password', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Password :',
                'label_attr'    => [
                    'class' => 'col-md-4 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Login',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }

    protected function buildLogout()
    {
        foreach($this->fields as $key => $value)
        {
            if($key!='submit') unset($this->fields[$key]);
        }

        $this
            ->add('submit', 'submit', [
                'label' => 'Logout',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
}