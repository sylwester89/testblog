<?php

namespace App\Forms\Handlers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;

use App\User;
use App\Forms\SignForm;
use App\Forms\RegisterForm;
use App\Forms\ResetPasswordForm;

class UserFormHandler
{
    private $formBuilder;
    private $request;
    private $signForm;
    private $registerForm;
    private $resetPasswordForm;
    private $user;

    public function __construct(
        FormBuilder $formBuilder,
        SignForm $signForm,
        RegisterForm $registerForm,
        ResetPasswordForm $resetPasswordForm
    )
    {
        $this->formBuilder = $formBuilder;
        $this->signForm = $signForm;
        $this->registerForm = $registerForm;
        $this->resetPasswordForm = $resetPasswordForm;
    }

    /**
     * @param string $type
     * @return Form
     */
    public function create(string $type = null) : Form
    {
        switch ($type) {
            case "login":
            case "logout":
                $formClass = $this->signForm;
                break;
            case "linkRequest":
            case "resetPassword":
                $formClass = $this->resetPasswordForm;
                break;
            default:
                $formClass = $this->registerForm;
        }

        return $this->formBuilder->create(
            $formClass->getClassName(), $formClass->getOptions($type)
        );
    }

    /**
     * @param Request $request
     * @return User
     */
    public function handle(Request $request) : User
    {
        $this->request = $request;
        $this->user = new User();

        return $this->validate()->save();
    }

    /**
     * @return UserFormHandler
     */
    private function validate() : UserFormHandler
    {
        $this->request->validate([
            'name' => 'required|string|min:2|max:60',
            'email' => 'required|email|min:6|max:60|unique:users',
            'password' => 'required|string|min:6|max:60|confirmed',
        ]);

        return $this;
    }

    /**
     * @return User
     */
    private function save() : User
    {
        $this->request->merge(
            ['password' => bcrypt($this->request->get('password'))]
        );

        $this->user->fill($this->request->all());
        $this->user->save();

        return $this->user;
    }
}
