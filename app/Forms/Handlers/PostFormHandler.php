<?php

namespace App\Forms\Handlers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;

use App\Post;
use App\Forms\PostForm;

class PostFormHandler
{
    private $formBuilder;
    private $request;
    private $postForm;
    private $post;

    public function __construct(
        FormBuilder $formBuilder,
        PostForm $postForm
    )
    {
        $this->formBuilder = $formBuilder;
        $this->postForm = $postForm;
    }

    /**
     * @param string $type
     * @return Form
     */
    public function create(string $type = null) : Form
    {
        switch ($type) {
            default:
                $formClass = $this->postForm;
        }

        return $this->formBuilder->create(
            $formClass->getClassName(), $formClass->getOptions($type)
        );
    }

    /**
     * @param Request $request
     * @return Post
     */
    public function handle(Request $request) : Post
    {
        $this->request = $request;
        $this->post = new Post();

        return $this->validate()->save();
    }

    /**
     * @return PostFormHandler
     */
    private function validate() : PostFormHandler
    {
        $this->request->validate([
            'title' => 'required|string|min:2|max:60',
            'description' => 'required|string|min:6|max:60'
        ]);

        return $this;
    }

    /**
     * @return Post
     */
    private function save() : Post
    {
        $this->request->merge(array_merge(
            ['user_id' => auth()->user()->id], $this->request->all()
        ));

        $this->post->fill($this->request->all());
        $this->post->save();

        return $this->post;
    }
}
