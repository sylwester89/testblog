<?php

namespace App\Forms\Handlers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;

use App\Comment;
use App\Forms\CommentForm;

class CommentFormHandler
{
    private $formBuilder;
    private $request;
    private $commentForm;
    private $comment;

    public function __construct(
        FormBuilder $formBuilder,
        CommentForm $commentForm
    )
    {
        $this->formBuilder = $formBuilder;
        $this->commentForm = $commentForm;
    }

    /**
     * @param string $type
     * @return Form
     */
    public function create(string $type = null) : Form
    {
        switch ($type) {
            default:
                $formClass = $this->commentForm;
        }

        return $this->formBuilder->create(
            $formClass->getClassName(), $formClass->getOptions($type)
        );
    }

    /**
     * @param Request $request
     * @return Comment
     */
    public function handle(Request $request) : Comment
    {
        $this->request = $request;
        $this->comment = new Comment();

        return $this->validate()->save();
    }

    /**
     * @return CommentFormHandler
     */
    private function validate() : CommentFormHandler
    {
        $this->request->validate([
            'content' => 'required|string|min:6|max:60'
        ]);

        return $this;
    }

    /**
     * @return Comment
     */
    private function save() : Comment
    {
        $this->request->merge(array_merge(
            ['user_id' => auth()->user()->id], $this->request->all()
        ));

        $this->comment->fill($this->request->all());
        $this->comment->save();

        return $this->comment;
    }
}
