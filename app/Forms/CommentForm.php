<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CommentForm extends Form
{
    protected $options = [
        'method' => 'POST'
    ];

    public function getClassName()
    {
        return self::class;
    }

    public function getOptions()
    {
        $this->options['url'] = route('admin.comments.store');

        return $this->options;
    }

    public function buildForm()
    {
        $this
            ->add('post_id', 'hidden')
            ->add('content', 'textarea', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Comment :',
                'label_attr'    => [
                    'class' => 'col-md-4 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Put a comment',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
}