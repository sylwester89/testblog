<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class PostForm extends Form
{
    protected $options = [
        'method' => 'POST'
    ];

    public function getClassName()
    {
        return self::class;
    }

    public function getOptions()
    {
        $this->options['url'] = route('admin.posts.store');

        return $this->options;
    }

    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'rules' => 'required|email|min:6|max:60',
                'label'    => 'Title :',
                'label_attr'    => [
                    'class' => 'col-md-4 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('description', 'textarea', [
                'rules' => 'required|min:6|max:60',
                'label'    => 'Description :',
                'label_attr'    => [
                    'class' => 'col-md-4 control-label'
                ],
                'attr'  => [
                    'required' => 'required',
                    'class' => 'form-control',
                    'maxlength' => 60
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Create a Post',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
}