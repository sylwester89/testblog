<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Routing\Router;

class MenuComposer
{
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $currentRouteActionBaseName = class_basename($this->router->currentRouteAction());

        if(strpos($currentRouteActionBaseName, 'Controller@') !== false)
            list($controller, $action) = explode('Controller@', $currentRouteActionBaseName);

        $view
            ->with('controller', $controller ?? null)
            ->with('action', $action ?? null);
    }
}