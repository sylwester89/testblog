<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

use App\Repositories\PostRepository;

class SupplyComposer
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view
            ->with('posts', $this->postRepository->allRestricted());
    }
}