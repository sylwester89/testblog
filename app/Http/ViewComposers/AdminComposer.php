<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Http\Request;

use App\Forms\Handlers\UserFormHandler;

class AdminComposer
{
    private $request;
    private $userFormHandler;

    public function __construct(Request $request, UserFormHandler $userFormHandler)
    {
        $this->request = $request;
        $this->userFormHandler = $userFormHandler;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view
            ->with('user',  $this->request->user())
            ->with('logoutForm',  $this->userFormHandler->create('logout')
                ->setFormOptions(['type' => 'logout'])
                ->rebuildForm()
            );
    }
}