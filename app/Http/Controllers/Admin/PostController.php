<?php

namespace App\Http\Controllers\Admin;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Post;
use App\Http\Controllers\Controller;
use App\Forms\Handlers\PostFormHandler;
use App\Forms\Handlers\CommentFormHandler;

class PostController extends Controller
{
    private $templatePath = 'admin.posts.';

    private $postFormHandler;
    private $commentFormHandler;

    public function __construct(
        PostFormHandler $postFormHandler,
        CommentFormHandler $commentFormHandler
    )
    {
        $this->postFormHandler = $postFormHandler;
        $this->commentFormHandler = $commentFormHandler;
    }

    /**
     * Display a listing of the Posts.
     *
     * @return View
     */
    public function index() : View
    {
        return view("{$this->templatePath}index");
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return View
     */
    public function create() : View
    {
        return view("{$this->templatePath}create", [
            'postForm' => $this->postFormHandler->create()
        ]);
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request) : RedirectResponse
    {
        $post = $this->postFormHandler->handle($request);
        if(!$post)
        {
            return redirect(route('admin.posts.index'))->withMessage([
                'type'    => 'error',
                'message' => 'Something goes wrong'
            ]);
        }

        return redirect(route('admin.posts.index'))->withMessage([
            'type'    => 'success',
            'message' => 'Action done'
        ]);
    }

    /**
     * Show specific Post.
     *
     * @param  Post  $post
     * @return View
     */
    public function show(Post $post) : View
    {
        return view("{$this->templatePath}show", compact('post'))
            ->with('commentForm', $this->commentFormHandler->create());
    }
}
