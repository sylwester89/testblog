<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Http\Controllers\Controller;
use App\Forms\Handlers\CommentFormHandler;

class CommentController extends Controller
{
    private $commentFormHandler;

    public function __construct(CommentFormHandler $commentFormHandler)
    {
        $this->commentFormHandler = $commentFormHandler;
    }

    /**
     * Store a newly created Comment in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request) : RedirectResponse
    {
        $comment = $this->commentFormHandler->handle($request);
        if(!$comment)
        {
            return back()->withMessage([
                'type'    => 'error',
                'message' => 'Something goes wrong'
            ]);
        }

        return back()->withMessage([
            'type'    => 'success',
            'message' => 'Action done'
        ]);
    }
}
