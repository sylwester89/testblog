<?php

namespace App\Http\Controllers\Admin;

use Illuminate\View\View;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    private $templatePath = 'admin.dashboard.';

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function __invoke() : View
    {
        return view("{$this->templatePath}index");
    }
}
