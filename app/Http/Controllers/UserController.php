<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use App\Forms\Handlers\UserFormHandler;

class UserController extends Controller
{
    use RegistersUsers, AuthenticatesUsers, SendsPasswordResetEmails, ResetsPasswords {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers, ResetsPasswords;
        AuthenticatesUsers::guard insteadof RegistersUsers, ResetsPasswords;
        ResetsPasswords::credentials insteadof AuthenticatesUsers;
        ResetsPasswords::broker insteadof SendsPasswordResetEmails;
    }

    private $templatePath = 'frontend.users.';

    /**
     * Where to redirect users after each action.
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $userFormHandler;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserFormHandler $userFormHandler)
    {
        $this->userFormHandler = $userFormHandler;
    }

    /**
     * Show the application registration form.
     *
     * @return View
     */
    public function showRegistrationForm() : View
    {
        return view("{$this->templatePath}register", [
            'registerForm' => $this->userFormHandler->create()
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $user = $this->userFormHandler->handle($request);

        return $this->registered($request, $user)
            ?: back()->withMessage([
                    'type'    => 'error',
                    'message' => 'Something goes wrong'
            ]);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
       return back()->withMessage([
               'type'    => 'success',
               'message' => "Action done. You can now log in to {$user->name} account"
           ]);
    }

    /**
     * Show the application's login form.
     *
     * @return View
     */
    public function showLoginForm() : View
    {
        return view("{$this->templatePath}login", [
            'loginForm' => $this->userFormHandler->create('login')
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param Request  $request
     * @return RedirectResponse
     */
    public function logout(Request $request) : RedirectResponse
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect($this->redirectTo);
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return View
     */
    public function showLinkRequestForm() : View
    {
        return view("{$this->templatePath}email", [
            'linkRequestForm' => $this->userFormHandler->create('linkRequest')
        ]);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  Request  $request
     * @param  string|null  $token
     * @return View
     */
    public function showResetForm(Request $request, $token = null) : View
    {
        return view("{$this->templatePath}reset")->with([
                'token' => $token,
                'email' => $request->email,
                'resetPasswordForm' => $this->userFormHandler->create('resetPassword')
            ]
        );
    }
}
