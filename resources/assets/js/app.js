
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var vueComponents = [
    'loading-effect'
];

vueComponents.forEach(function(componentName) {
    let componentFileNameSplitted = componentName.split('-');
    let componentFileNameFirstPart =
        componentFileNameSplitted[0].substring(0, 1).toUpperCase()+componentFileNameSplitted[0].substring(1);
    let componentFileNameSecondPart =
        componentFileNameSplitted[1].substring(0, 1).toUpperCase()+componentFileNameSplitted[1].substring(1);
    let componentFileName = componentFileNameFirstPart+componentFileNameSecondPart;
    console.log(componentName, './components/'+componentFileName+'.vue');
    Vue.component(componentName, require('./components/'+componentFileName+'.vue'));
});

const app = new Vue({
    el: '#v-app-1'
});