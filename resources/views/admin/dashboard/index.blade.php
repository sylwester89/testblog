@extends('layouts.app')

@section('content')
    <div class="card my-4">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            You are logged in!
        </div>
    </div>

    <div class="card my-4">
        <div class="card-header">{{ $user->comments->count()>0 ? 'Your comments :' : 'You do not have any comments yet' }}</div>

        @foreach($user->comments as $comment)
            <!-- Single Comment -->
            <div class="media mb-4">
                <img class="ml-3 mt-3 d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                    <h5 class="mt-3">{{ $user->name }}</h5>
                    {{ $comment->content }}
                </div>
            </div>
        @endforeach
    </div>
@endsection
