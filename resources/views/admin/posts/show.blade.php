@extends('layouts.app')

@section('content')

        <!-- Title -->
        <h1 class="mt-4">
            {{ $post->title }}
        </h1>

        <hr>
        <!-- Date/Time -->
        <p>
            Posted on {{ $post->created_at->format('M d, Y, H:i:s') }}
        </p>

        <hr>
        <!-- Post Content -->
        <p class="lead">
            {{ $post->description }}
        </p>

        <hr>
        <!-- Comments Form -->
        <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                {!! form_start($commentForm) !!}
                    {!! form_row($commentForm->post_id, ['value' => $post->id ]) !!}
                {!! form_end($commentForm) !!}
            </div>
        </div>

        @foreach($post->comments as $comment)
            <!-- Single Comment -->
            <div class="media mb-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                    <h5 class="mt-0">{{ $comment->user->name }}</h5>
                    {{ $comment->content }}
                </div>
            </div>
        @endforeach
@endsection