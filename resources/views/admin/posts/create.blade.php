@extends('layouts.app')

@section('content')
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Post') }}</div>

                <div class="card-body">
                    {!! form($postForm) !!}
                </div>
            </div>
        </div>
    </div>
@endsection