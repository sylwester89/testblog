@extends('layouts.app')

@section('content')

    <h1 class="my-4">
        @if($posts->count()>0)
            All posts
                <small>with comments inside</small>
        @else
            There is no posts.
        @endif
    </h1>

    @foreach($posts as $post)
        <!-- Blog Post -->
        <div class="card mb-4">
            <div class="card-body">
                <h2 class="card-title">{{ $post->title }}</h2>
                <p class="card-text">{{ $post->description }}</p>
                <a href="{{ route('admin.posts.show', ['post' => $post->id]) }}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
                Posted on {{ $post->created_at->format('M d, Y, H:i:s') }}
            </div>
        </div>
    @endforeach
@endsection