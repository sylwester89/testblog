<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Test Blog">
    <meta name="author" content="Sylwester Harko">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="v-app-1">

        @include('errors.index')

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top mt-1">
            <div class="container">
                @guest
                    <a class="navbar-brand" href="{{ route('login') }}">{{ config('app.name', 'Test Blog') }}</a>
                @endguest
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        @guest
                            <li class="nav-item {!! $controller == 'User' && $action=='showLoginForm' ? 'active' : '' !!}">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item {!! $controller == 'User' && $action=='showRegistrationForm' ? 'active' : '' !!}">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="btn btn-warning" href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="nav-item ml-2">
                                <a class="btn btn-success" href="{{ route('admin.posts.index') }}">Posts</a>
                            </li>
                            <li class="nav-item ml-2">
                                {!! form($logoutForm) !!}
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active">{{ $user->name }}</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <!-- Blog Entries Column -->
                <div class="col-md-8">
                    @yield('content')
                </div>

                <!-- Sidebar Widgets Column -->
                <div class="col-md-4">
                    @guest
                        <!-- Categories Widget -->
                        <div class="card my-4">
                            <h5 class="card-header">Posts</h5>
                            <div class="card-body">
                                <div class="row">
                                    @if($posts->count()>0)
                                        @foreach($posts as $post)
                                            <div class="col-lg-6">
                                                <ul class="list-unstyled mb-0">
                                                    <li>
                                                       {{ $post->title }}
                                                    </li>
                                                </ul>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-lg-6">
                                            <ul class="list-unstyled mb-0">
                                                <li>
                                                    No posts at all.
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endguest
                    <!-- Side Widget -->
                    <div class="card my-4">
                        <h5 class="card-header">Side Widget</h5>
                        <div class="card-body">
                            You can post and comment on this simple blog ! Try it.
                        </div>
                        @auth
                            <a class="btn btn-primary" href="{{ route('admin.posts.create') }}">Create a Post</a>
                        @endauth
                    </div>

                </div>

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

        <!-- Footer -->
        <footer class="py-3 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; {{ config('app.name', 'Test Blog') }} 2018</p>
            </div>
            <!-- /.container -->
        </footer>

        <loading-effect></loading-effect>
    </div>

    @section('scripts')
        <script src="{{ asset('js/app.js') }}"></script>
    @show
</body>
</html>