@if($errors->any())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        @foreach($errors->all() as $error)
            <div class="text-center">{{ $error }}</div>
        @endforeach
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-{{ session()->get('message.type') }}">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="text-center">{{ session()->get('message.message') }}</div>
    </div>
@endif