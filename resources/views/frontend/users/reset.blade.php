@extends('layouts.app')

@section('content')
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    {!! form_start($resetPasswordForm) !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                    {!! form_end($resetPasswordForm) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
