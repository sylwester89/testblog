@extends('layouts.app')

@section('content')
    <div class="row justify-content-center my-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        {!! form($linkRequestForm) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
