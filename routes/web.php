<?php

$this->redirect('/', '/admin');

$this->post('logout',  ['uses' => 'UserController@logout', 'as' => 'logout']);

$this->group(['middleware' => 'guest'], function ()
{
    $this->get('login', ['uses' => 'UserController@showLoginForm', 'as' => 'login']);
    $this->post('login', ['uses' => 'UserController@login']);

    $this->get('register', ['uses' => 'UserController@showRegistrationForm', 'as' => 'register']);
    $this->post('register', ['uses' => 'UserController@register']);

    $this->group(['prefix' => 'password'], function () {
        $this->get('reset', ['uses' => 'UserController@showLinkRequestForm', 'as' => 'password.request']);
        $this->post('email', ['uses' => 'UserController@sendResetLinkEmail', 'as' => 'password.email']);
        $this->get('reset/{token}', ['uses' => 'UserController@showResetForm', 'as' => 'password.reset']);
        $this->post('reset', ['uses' => 'UserController@reset']);
    });
});

$this->group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'auth'], function ()
{
    $this->get('/', ['uses' => 'DashboardController', 'as' => 'dashboard']);

    $this->resource('posts', 'PostController')->except([
        'edit', 'update', 'destroy'
    ]);

    $this->apiResource('comments', 'CommentController')->only([
        'store'
    ]);
});
